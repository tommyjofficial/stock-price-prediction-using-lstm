# Stock price prediction using LSTM

### This project implements LSTM (Long Short Term Memory) Neural Network to predict future stock prices based on previous values in the timeseries. This project gets historic data from Yahoo Finance and calculates preditions for a specified time period. 

### This model predicts a series of future price values for a specified period, using a specified period of historic values. The period of price prediction, used in this example, is 5 days based on 30-day period of historic data, (the interval is 1 day). However, any periods and intervals can be used within the project (need to be specified via variables).

### The model uses "tensorflow" for Neural Network models, "yahooquery" for stock data, "pandas" and "numpy" for storing and processing data and "plotly" for data visualisation. The framework of choice is Jupyter Notebook.

### Conclusions:
### The model is quite accurate when the data is behaving normally (no noise in the data), i.e. price is constant: steadily rising or falling. However it can not predict sudden movements within data, which is essentially noisy data.